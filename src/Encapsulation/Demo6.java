package Encapsulation;

public class Demo6 {
    static Demo6 d1;
    static int count=0;

    private Demo6(){
        count++;
    }

    public static Demo6 createObject(){
      if(count==0){
          d1=new Demo6();
      }
        return d1;
    }

}
