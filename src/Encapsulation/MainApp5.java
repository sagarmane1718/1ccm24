package Encapsulation;

public class MainApp5 {
    public static void main(String[] args) {
        Application5 app=new Application5();
        app.display();

        Java5 jse=new Java5() {
            @Override
            public void display() {
                System.out.println("Application developed by using Python");
            }
        };
        jse.display();

        Java5 jme=new Java5() {
            @Override
            public void display() {
                System.out.println("Application developed by using Android ");
            }
        };
        jme.display();
    }
}
