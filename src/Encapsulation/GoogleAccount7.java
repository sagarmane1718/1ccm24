package Encapsulation;

public class GoogleAccount7 {
    static GoogleAccount7 gmail;
    static int count=0;

    private GoogleAccount7(){
        count++;
    }

    public static GoogleAccount7 Gmail(){
        if(count==0){
            gmail=new GoogleAccount7();
        }
        return gmail;
    }

    void GooglePhotos(){
        System.out.println("View & Upload Photos");
    }

    void GoogleDrive(){
        System.out.println("Download/Upload Document");
    }
}
