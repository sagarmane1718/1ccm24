package Encapsulation;

public class MainApp4 {
    public static void main(String[] args) {
        Mobile4 m1=new Mobile4();

        Mobile4.Processor mp=m1.new Processor();
        mp.display();

        Mobile4.Ramsize mr=m1.new Ramsize();
        mr.display();
    }
}
